extends KinematicBody2D

var velocity = Vector2()
var moveTarg = position
var usingPad = false
var usingPointer = false
export (float) var stopDistance = 5
export (float) var speed = 100
var mousePos = position
export (bool) var enable_click_to_move = true
export (bool) var enable_drag_to_move = true
export (bool) var enable_buttons_to_move = true
var moving = false
var can_move = true
var keyDown = false
onready var sprite = $"AnimatedSprite"
export (String) var up_anim_name = "up"
export (String) var down_anim_name = "down"
export (String) var left_anim_name = "left"
export (String) var right_anim_name = "right"
export (String) var up_left_anim_name = "up left"
export (String) var down_right_anim_name = "down right"
export (String) var down_left_anim_name = "down left"
export (String) var up_right_anim_name = "up right"
export (String) var idle_animation_name = "idle"
var lastDir = Vector2()
onready var interacter = $"interacter"
export (float) var interact_distance = .2
var selected_object
var moveDir = Vector2()
var speedPercent = 0

var anim_dictionary = {
"u":up_anim_name, "d":down_anim_name,
"l":left_anim_name, "r":right_anim_name,
"lu":up_left_anim_name, "rd":down_right_anim_name,
"ld":down_left_anim_name, "ru":up_right_anim_name,
"":idle_animation_name
}

signal stopped

func interact():
	pass

func dirDetect():
	var c = {Vector2.LEFT:"l",Vector2.RIGHT:"r",Vector2.UP:"u",Vector2.DOWN:"d"}
	var dir = ""
	for k in c:
		if lastDir.normalized().dot(k) > .4:
			dir += c[k]
	return dir

func playAnim():
	if speedPercent != 0:
		sprite.speed_scale = speedPercent
	sprite.play(anim_dictionary[dirDetect()])

var directionalInputs = {"up":Vector2.UP,"down":Vector2.DOWN,"left":Vector2.LEFT,"right":Vector2.RIGHT}
var d_input_v = directionalInputs.values()
var d_input_k = directionalInputs.keys()

func directional_button_move():
	if enable_buttons_to_move:
		var iter = 0
		moveDir = Vector2()
		for i in d_input_k:
			if Input.is_action_pressed(i):
				moveDir += (d_input_v[iter])*Input.get_action_strength(i)
			if iter <= d_input_k.size():
				iter+=1
			else:
				iter = 0
		if moveDir != Vector2():
			velocity = moveDir
			moveTarg = position
#		moveTarg += velocity.normalized()
#			velocity = velocity.normalized()

func click_move():
	if enable_click_to_move:
		if Input.is_action_pressed("click"):
			moveTarg = $"..".get_local_mouse_position()
		if Input.is_action_just_released("click"):#when the click is released, check whether we're using click and move or click and hold
			if enable_drag_to_move and !enable_click_to_move:
				moveTarg = position
				velocity = Vector2()
		if moveDir == Vector2():
			velocity += (moveTarg - position).normalized()
		if position.distance_to(moveTarg) <= stopDistance and !Input.is_action_pressed("click") and moveDir == Vector2():
			velocity = Vector2()

func moveInput():
	if can_move:
		directional_button_move()
		click_move()
	if velocity.x > velocity.y:
		speedPercent = velocity.x
	else:
		speedPercent = velocity.y
	if velocity == Vector2():
		emit_signal("stopped")

func _ready():
	pass

#func findBiggest(v2):
#	var res = v2.x
#	var res2 = v2.y
#	var both = [res,res2]
#	for i in both:
#		if i < 0:
#			i*=-1
#	if res >= res2:
#		return res
#	else:
#		return res2

# warning-ignore:unused_argument
func _process(delta):
	moveInput()
	if velocity.x > 1 or velocity.y > 1 or velocity.x < -1 or velocity.y < -1:
		velocity = velocity.normalized()
	lastDir = move_and_slide(velocity*speed, Vector2.UP)
	if lastDir != Vector2():
		interacter.position = lastDir*interact_distance
	playAnim()
	pass


func _on_interacter_body_entered(body):
	if body.is_in_group("interactive"):
		selected_object = body
		print("hovering")
	pass # Replace with function body.


func _on_interacter_body_exited(_body):
	selected_object = null
	pass # Replace with function body.


func _on_Node2D_stopped():
	pass # Replace with function body.
